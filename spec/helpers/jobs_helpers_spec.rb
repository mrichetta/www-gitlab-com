require 'spec_helper'
require_relative '../../helpers/jobs_helpers'

describe JobsHelpers do
  class FakeTemplate
    extend JobsHelpers
  end

  # "arr", "result", and "teams" parameters are only used recursively;
  # they are always empty when passed for the public interface.
  let(:arr) { [] }
  let(:result) { [] }
  let(:teams) { [] }

  let(:child_department) do
    {
      id: 4043191002,
      name: "Business Development",
      parent_id: 4011049002,
      child_ids: [],
      jobs: [
        {
          absolute_url: "https://boards.greenhouse.io/gitlab/jobs/4521747002",
          internal_job_id: 4418290002,
          location: { name: "Remote - US, West Coast preferred" },
          metadata: [
            { id: 4694722002,
              name: "Hiring Plan",
              value: "Addition to Plan",
              value_type: "single_select" },
            { id: 4595831002,
              name: "Quota Coverage Type",
              value: "n/a",
              value_type: "single_select" }
          ],
          id: 4521747002,
          updated_at: "2020-01-10T12:32:27-05:00",
          requisition_id: "1005",
          title: "Technology Partner Manager (Microsoft Lead)"
        }
      ]
    }
  end
  let(:parent_department) do
    {
      id: 4011049002,
      name: "Sales",
      parent_id: nil,
      child_ids: [
        4043191002
      ],
      jobs: []
    }
  end
  let(:departments_and_jobs_data) { [child_department, parent_department] }

  describe '#show_childs' do
    describe 'when show_list is false' do
      let(:show_list) { false }
      let(:depth) { 1 }

      it 'returns nested department data for dropdown options' do
        child_department_data = FakeTemplate.show_childs(
          departments_and_jobs_data,
          parent_department,
          arr,
          show_list,
          depth,
          parent_department[:id],
          result,
          teams
        )

        expected_data = [{ id: 4043191002, name: "&#160;Business Development&#160;" }]
        expect(child_department_data).to eq(expected_data)
      end
    end

    describe 'when show_list is true' do
      let(:show_list) { true }
      let(:depth) { 2 }

      it 'returns teams and jobs HTML' do
        child_department_data = FakeTemplate.show_childs(
          departments_and_jobs_data,
          parent_department,
          arr,
          show_list,
          depth,
          parent_department[:id],
          result,
          teams
        )

        expected_team_html =
          "<h5 class='team-title' data-team='4043191002' style='margin-left: 20px'>" \
            "<b>Business Development</b>" \
            "</h5>"
        expected_job_html =
          "<div class='job-container' style='margin-left: 40px' data-teams='[4043191002, 4011049002]' data-depth='4'>" \
            "<a href='/jobs/apply/technology-partner-manager-microsoft-lead-4521747002'>" \
            "Technology Partner Manager (Microsoft Lead)" \
            "</a>" \
            "<h6>Remote - US, West Coast preferred</h6>" \
            "</div>"
        expected_data = [expected_team_html, expected_job_html]
        expect(child_department_data).to eq(expected_data)
      end
    end
  end
end
